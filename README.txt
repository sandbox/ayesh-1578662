This tiny module will redirect comment permalink to their corresponding 
node paths.

For an example,
Imagine there are 3 comments for the node with ID 123. The node URL 
would be http://example.com/node/123.
If there are 3 comments to the node(with comment IDs 145, 156 and 175), 
they will have URLs like 
 - http://example.com/comment/145
 - http://example.com/comment/156
 - http://example.com/comment/175.

But all 4 URLs above give the same content to the end user. This raises 
a possibility that search engines will detect duplicate content in your site.

This module will redirect all comment/x paths to their corresponding node-path.

  http://example.com/comment/145 to http://example.com/node/123#comment-145
  http://example.com/comment/156 to http://example.com/node/123#comment-156

This module can handle pagers as well. If your comment is in the second page
(when you have set a comments-per-page value in node type settings),
http://example.com/comment/156 to http://example.com/node/123?page=1#comment-156

However, please note that this module won't rewrite comment permalinks to
visible in comment's title, Views, core recent comments block or anywhere else.
When a page is requested, this module will redirect the user to the correct 
node page.

HTTP redirect response is 301 Moved Permanently.

This module doesn't provide any user interface. It simply overrides
core comment permalink page callback and provides a new one with
drupal_goto() to the node path.
To enable the functionality, enable the module. 
Disable the module to disable this functionality. 
